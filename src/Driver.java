import Form.NOM;

/**
 * Created by Nc5xb3 on 7/07/2014.
 */
public class Driver {
    public static void main(String[] args) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                java.awt.EventQueue.invokeLater(new Runnable() {
                    public void run() {
                        new NOM().setVisible(true);
                    }
                });
            }
        });
        t.start();
    }
}
