package Model;

/**
 * Created by Nc5xb3 on 9/07/2014.
 */
public class Link {
    private String url;
    private String sectionName;
    private boolean isImage;
    private String downloadedPath;

    public Link(String url, String sectionName, boolean isImage) {
        this.url = url;
        this.sectionName = sectionName;
        this.isImage = isImage;
        downloadedPath = "";
    }

    public String getUrl() {
        return url;
    }

    public String getSectionName() {
        return sectionName;
    }

    public boolean isImage() {
        return isImage;
    }

    public void setDownloadedPath(String filename) {
        this.downloadedPath = filename;
    }

    public String getDownloadedPath() {
        return this.downloadedPath;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Link link = (Link) o;

        if (isImage != link.isImage) return false;
        if (sectionName != null ? !sectionName.equals(link.sectionName) : link.sectionName != null) return false;
        if (url != null ? !url.equals(link.url) : link.url != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = url != null ? url.hashCode() : 0;
        result = 31 * result + (sectionName != null ? sectionName.hashCode() : 0);
        result = 31 * result + (isImage ? 1 : 0);
        return result;
    }
}
