package Model;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Created by Nc5xb3 on 8/07/2014.
 */
public class Unit implements Comparable<Unit> {
    private String code;
    private String title;
    private String url;
    private String text;
    private String activityText;
    private Set<Link> links;

    public Unit(String code, String title, String url) {
        this.code = code;
        this.title = title;
        this.url = url;
        this.activityText = "";
        links = new HashSet<Link>();
    }

    public String getCode() {
        return code;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void addLink(Link link) {
        links.add(link);
    }

    public Set<Link> getLinks() {
        return links;
    }

    public String getActivityText() {
        return activityText;
    }

    public void setActivityText(String activityText) {
        this.activityText = activityText;
    }

    @Override
    public int compareTo(Unit o) {
        return code.compareTo(o.code);
    }
}
