package Form;

import Model.Unit;
import org.apache.commons.lang3.StringUtils;
import org.apache.xpath.operations.Bool;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nc5xb3 on 21/09/2014.
 */
public class Settings extends JDialog {

    private List<Unit> units;

    public Settings() {
        super(new javax.swing.JFrame(), true);
        initComponents();
    }

    public void setUnits(List<Unit> units, String selectedUnits) {
        this.units = units;

        tblUnits.removeAll();
        DefaultTableModel model = (DefaultTableModel) tblUnits.getModel();
        int rowCount = model.getRowCount();

        for (int i = rowCount - 1; i >= 0; i--) model.removeRow(i);

        String[] sUnits = selectedUnits.split(",");

        for (Unit unit : units) {
            Boolean selected = false;
            for (String su : sUnits) {
                if (su.trim().equalsIgnoreCase(unit.getCode())) {
                    selected = true;
                    break;
                }
            }
            model.addRow(new Object[]{selected, unit.getCode(), unit.getTitle()});
        }
        tblUnits.setModel(model);
    }
    public String getSelectedUnits() {
        String units = "";
        DefaultTableModel model = (DefaultTableModel) tblUnits.getModel();
        int col_sync = -1, col_code = -1;
        for (int i = 0; i < model.getColumnCount(); i++) {
            if (model.getColumnName(i).equals("Sync")) {
                col_sync = i;
            } else if (model.getColumnName(i).equals("Unit Code")) {
                col_code = i;
            }
        }
        for (int i = 0; i < model.getRowCount(); i++)
            if ((Boolean) model.getValueAt(i, col_sync))
                units += "," + model.getValueAt(i, col_code);

        return units.replaceFirst(",", "");
    }

    public void setTerminateSyncTotalDownload(boolean checked, int value) {
        cbTerminateSyncTotalDownload.setSelected(checked);
        txtTerminateSyncTotalDownload.setText(String.valueOf(value));
    }
    public String getTerminateSyncTotalDownload() {
        return String.valueOf(cbTerminateSyncTotalDownload.isSelected());
    }
    public String getTerminateSyncTotalDownloadValue() {
        return txtTerminateSyncTotalDownload.getText();
    }

    public void setSkipFileDownload(boolean checked, int value) {
        cbSkipFileDownload.setSelected(checked);
        txtSkipFileDownload.setText(String.valueOf(value));
    }
    public String getSkipFileDownload() {
        return String.valueOf(cbSkipFileDownload.isSelected());
    }
    public String getSkipFileDownloadValue() {
        return txtSkipFileDownload.getText();
    }

    public void setOpenFolderAfterSync(boolean checked) {
        cbOpenFolderAfterSync.setSelected(checked);
    }
    public String getOpenFolderAfterSync() {
        return String.valueOf(cbOpenFolderAfterSync.isSelected());
    }

    public void setCloseAppAfterSync(boolean checked) {
        cbCloseAppAfterSync.setSelected(checked);
    }
    public String getCloseAppAfterSync() {
        return String.valueOf(cbCloseAppAfterSync.isSelected());
    }

    public void setHideDialog(boolean checked) {
        cbHideDialog.setSelected(checked);
    }
    public String getHideDialog() {
        return String.valueOf(cbHideDialog.isSelected());
    }


    private void initComponents() {

        lblUnits = new javax.swing.JLabel();
        spUnits = new javax.swing.JScrollPane();
        tblUnits = new javax.swing.JTable();
        cbTerminateSyncTotalDownload = new javax.swing.JCheckBox();
        txtTerminateSyncTotalDownload = new javax.swing.JTextField();
        cbSkipFileDownload = new javax.swing.JCheckBox();
        txtSkipFileDownload = new javax.swing.JTextField();
        cbOpenFolderAfterSync = new javax.swing.JCheckBox();
        cbCloseAppAfterSync = new javax.swing.JCheckBox();
        cbHideDialog = new javax.swing.JCheckBox();
        btnSet = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Settings");
        setResizable(false);

        lblUnits.setText("Units to sync with");

        tblUnits.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
            },
            new String [] {
                "Sync", "Unit Code", "Name"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Boolean.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                true, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        spUnits.setViewportView(tblUnits);
        tblUnits.getColumnModel().getColumn(0).setMinWidth(50);
        tblUnits.getColumnModel().getColumn(0).setPreferredWidth(50);
        tblUnits.getColumnModel().getColumn(0).setMaxWidth(50);
        tblUnits.getColumnModel().getColumn(1).setMinWidth(70);
        tblUnits.getColumnModel().getColumn(1).setPreferredWidth(70);
        tblUnits.getColumnModel().getColumn(1).setMaxWidth(70);

        cbTerminateSyncTotalDownload.setText("Terminate sync if total download is over # megabyte(s):");

        txtTerminateSyncTotalDownload.setText("256");

        cbSkipFileDownload.setText("Skip file download if it exceeds # megabyte(s):");

        txtSkipFileDownload.setText("16");

        cbOpenFolderAfterSync.setText("Open folder after synchronize");

        cbCloseAppAfterSync.setText("Close application after synchronize");

        cbHideDialog.setText("Hide Dialog");

        btnSet.setText("Set");
        btnSet.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                dispose();
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cbCloseAppAfterSync)
                    .addComponent(cbOpenFolderAfterSync)
                    .addComponent(spUnits, javax.swing.GroupLayout.DEFAULT_SIZE, 460, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(cbHideDialog)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnSet, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lblUnits)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cbTerminateSyncTotalDownload)
                            .addComponent(cbSkipFileDownload))
                        .addGap(14, 14, 14)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtTerminateSyncTotalDownload, javax.swing.GroupLayout.DEFAULT_SIZE, 151, Short.MAX_VALUE)
                            .addComponent(txtSkipFileDownload, javax.swing.GroupLayout.DEFAULT_SIZE, 151, Short.MAX_VALUE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblUnits)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(spUnits, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTerminateSyncTotalDownload, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbTerminateSyncTotalDownload, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtSkipFileDownload, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbSkipFileDownload, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cbOpenFolderAfterSync, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cbCloseAppAfterSync, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSet, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbHideDialog))
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }

    private javax.swing.JButton btnSet;
    private javax.swing.JCheckBox cbCloseAppAfterSync;
    private javax.swing.JCheckBox cbHideDialog;
    private javax.swing.JCheckBox cbOpenFolderAfterSync;
    private javax.swing.JCheckBox cbSkipFileDownload;
    private javax.swing.JCheckBox cbTerminateSyncTotalDownload;
    private javax.swing.JLabel lblUnits;
    private javax.swing.JScrollPane spUnits;
    private javax.swing.JTable tblUnits;
    private javax.swing.JTextField txtSkipFileDownload;
    private javax.swing.JTextField txtTerminateSyncTotalDownload;

}
