package Form;

import Configuration.Config;
import Connector.Connector;
import Connector.MonashConnector;

import javax.swing.text.DefaultCaret;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

/**
 * Created by Nc5xb3 on 21/09/2014.
 */
public class NOM extends javax.swing.JFrame {
    private Connector c;
    private Config config;

    final private String name;
    final private String version;

    public NOM() {
        name = "NOM";
        version = "1.0.6";
        initComponents();
        init();
    }

    private void init() {
        config = new Config();
        login_dialog.setUsername(config.data().getProperty("Username", ""));
    }

    private Login login_dialog;
    private Settings settings_dialog;
    private void initComponents() {
        login_dialog = new Login();
        settings_dialog = new Settings();

        spInfo = new javax.swing.JScrollPane();
        taInfo = new javax.swing.JTextArea();
        lblStatus = new javax.swing.JLabel();
        spData = new javax.swing.JScrollPane();
        taData = new javax.swing.JTextArea();
        pbProgress = new javax.swing.JProgressBar();
        btnSync = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle(name + " " + version);

        taInfo.setBackground(new java.awt.Color(236, 233, 216));
        taInfo.setColumns(20);
        taInfo.setEditable(false);
        taInfo.setFont(new java.awt.Font("Monospaced", 0, 12));
        taInfo.setRows(5);
        taInfo.setBorder(null);
        taInfo.setText(name + " " + version);
        taInfo.append("\nRecommended JRE: 1.8.0");
        taInfo.append("\nCurrent JRE: " + System.getProperty("java.version"));
        spInfo.setViewportView(taInfo);

        lblStatus.setText("Waiting to Sync");
        Dimension size = lblStatus.getPreferredSize();
        lblStatus.setMinimumSize(size);
        lblStatus.setPreferredSize(size);

        taData.setColumns(20);
        taData.setEditable(false);
        taData.setFont(new java.awt.Font("Monospaced", 0, 12));
        taData.setRows(5);
        taData.setLineWrap(true);
        spData.setViewportView(taData);

        DefaultCaret caret = (DefaultCaret) taData.getCaret();
        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

        pbProgress.setStringPainted(true);

        btnSync.setText("Sync");
        btnSync.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                c = new MonashConnector(taData, taInfo, lblStatus, pbProgress);

                settings_dialog.setTerminateSyncTotalDownload(
                        config.data().getProperty("TerminateSyncTotalDownload", "false").equals("true"),
                        Integer.parseInt(config.data().getProperty("TerminateSyncTotalDownloadValue", "256")));
                settings_dialog.setSkipFileDownload(
                        config.data().getProperty("SkipFileDownload", "false").equals("true"),
                        Integer.parseInt(config.data().getProperty("SkipFileDownloadValue", "16")));
                settings_dialog.setOpenFolderAfterSync(config.data().getProperty("OpenFolderAfterSync", "false").equals("true"));
                settings_dialog.setCloseAppAfterSync(config.data().getProperty("CloseAppAfterSync", "false").equals("true"));
                settings_dialog.setHideDialog(config.data().getProperty("HideDialog", "false").equals("true"));

                btnSync.setEnabled(false);
                // --
                String info = "Units syncing with:";
                for (String str : config.data().getProperty("Units", "").split(","))
                    info += "\n" + str;
                login_dialog.setTaInfo(info.trim());
                login_dialog.setShowDialogVisible(config.data().getProperty("HideDialog", "false").equals("true"));
                if (config.data().getProperty("Username", "").equals(""))
                    login_dialog.setShowDialog(true);
                // --
                login_dialog.setVisible(true);
                if (!new String(login_dialog.getPassword()).isEmpty()) {
                    Thread t = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                if (c.sync(login_dialog.getUsername(), new String(login_dialog.getPassword()))) {
                                    settings_dialog.setUnits(c.getUnits(), config.data().getProperty("Units", ""));
                                    if (login_dialog.getShowDialogSelected() || settings_dialog.getHideDialog().equals("false"))
                                        settings_dialog.setVisible(true);
                                    config.put("TerminateSyncTotalDownload", settings_dialog.getTerminateSyncTotalDownload());
                                    config.put("TerminateSyncTotalDownloadValue", settings_dialog.getTerminateSyncTotalDownloadValue());
                                    config.put("SkipFileDownload", settings_dialog.getSkipFileDownload());
                                    config.put("SkipFileDownloadValue", settings_dialog.getSkipFileDownloadValue());
                                    config.put("OpenFolderAfterSync", settings_dialog.getOpenFolderAfterSync());
                                    config.put("CloseAppAfterSync", settings_dialog.getCloseAppAfterSync());
                                    config.put("HideDialog", settings_dialog.getHideDialog());
                                    config.put("Units", settings_dialog.getSelectedUnits());
                                    config.put("Username", login_dialog.getUsername());
                                    config.save();
                                    c.download(
                                            settings_dialog.getSelectedUnits(),
                                            settings_dialog.getTerminateSyncTotalDownload().equals("true") ? Integer.parseInt(settings_dialog.getTerminateSyncTotalDownloadValue()) : -1,
                                            settings_dialog.getSkipFileDownload().equals("true") ? Integer.parseInt(settings_dialog.getSkipFileDownloadValue()) : -1
                                    );
                                    c.create_pages(settings_dialog.getSelectedUnits());
                                    if (settings_dialog.getOpenFolderAfterSync().equals("true"))
                                        if (Desktop.isDesktopSupported())
                                            Desktop.getDesktop().open(new File(c.getMainPath()));
                                    if (settings_dialog.getCloseAppAfterSync().equals("true"))
                                        System.exit(1);
                                }
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                            btnSync.setEnabled(true);
                        }
                    });
                    t.start();
                } else {
                    btnSync.setEnabled(true);
                }
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(spInfo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 620, Short.MAX_VALUE)
                    .addComponent(spData, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 620, Short.MAX_VALUE)
                    .addComponent(lblStatus, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 620, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(pbProgress, javax.swing.GroupLayout.DEFAULT_SIZE, 517, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSync, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(spInfo, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(spData, javax.swing.GroupLayout.DEFAULT_SIZE, 229, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSync, javax.swing.GroupLayout.DEFAULT_SIZE, 37, Short.MAX_VALUE)
                    .addComponent(pbProgress, javax.swing.GroupLayout.DEFAULT_SIZE, 37, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
        setLocationByPlatform(true);
        getRootPane().setDefaultButton(btnSync);
    }

    private javax.swing.JButton btnSync;
    private javax.swing.JLabel lblStatus;
    private javax.swing.JProgressBar pbProgress;
    private javax.swing.JScrollPane spData;
    private javax.swing.JScrollPane spInfo;
    private javax.swing.JTextArea taData;
    private javax.swing.JTextArea taInfo;

}
