package Utility;

/**
 * Created by Nc5xb3 on 21/09/2014.
 */
public class DownloadCounter {

    private int total_links, downloaded, exists, skipped, failed, cancelled;

    public DownloadCounter() {
        total_links = 0;
    }

    public void incrementLink(int n) {
        total_links += n;
    }

    public void incrementDownload() {
        downloaded++;
    }

    public void incrementExists() {
        exists++;
    }

    public void incrementSkipped() {
        skipped++;
    }

    public void incrementFailed() {
        failed++;
    }

    public void incrementCancelled() {
        cancelled++;
    }

    public int downloadProgress() {
        return downloaded + exists + skipped + failed + cancelled;
    }

    public int getTotalLinks() {
        return total_links;
    }

    public int getDownloaded() {
        return downloaded;
    }

    public int getExists() {
        return exists;
    }

    public int getSkipped() {
        return skipped;
    }

    public int getFailed() {
        return failed;
    }

    public int getCancelled() {
        return cancelled;
    }

}
