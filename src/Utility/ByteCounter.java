package Utility;

import java.io.IOException;
import java.text.DecimalFormat;

/**
 * Created by Nc5xb3 on 8/07/2014.
 */
public class ByteCounter {
    private double total;
    private int last_added;

    public ByteCounter() {
        total = 0;
        last_added = 0;
    }

    public void add(int length) {
        total += (last_added = length);
    }

    public String getTotalAsString(char format) throws IOException {
        return getStringForm(total, format);
    }

    public String getLastAddedAsString(char format) throws IOException {
        return getStringForm(last_added, format);
    }

    private static String getStringForm(double value, char format) throws IOException {
        DecimalFormat formatter = new DecimalFormat("#,##0.00");
        String unit = "byte";
        switch (format) {
            case 'b':
                formatter = new DecimalFormat("#,###");
                break;
            case 'k':
                value /= Math.pow(2, 10);
                unit = "kilobyte";
                break;
            case 'm':
                value /= Math.pow(2, 20);
                unit = "megabyte";
                break;
            case 'g':
                value /= Math.pow(2, 30);
                unit = "gigabyte";
                break;
            default:
                throw new IOException("Invalid format " + format);
        }
        if (value != 1) unit += "(s)";
        return formatter.format(value) + " " + unit;
    }
}
