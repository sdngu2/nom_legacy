package Connector;

import java.io.*;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;

/**
 * Created by Nc5xb3 on 10/07/2014.
 */
public class FileStreamWriter {

    static final private int ktom = 1024 * 1024;

    public static boolean writeInputStream(InputStream contentAsStream, String outputFile, double skipFileDownloadValue) {
        OutputStream outputStream = null;
        File file = null;
        int total_bytes = 0;

        try {
            file = new File(outputFile);
            file.getParentFile().mkdirs();

            outputStream = new FileOutputStream(file);

            int read;
            byte[] bytes = new byte[1024 * 8];

            while ((read = contentAsStream.read(bytes)) != -1) {
                total_bytes += read;
                if (skipFileDownloadValue > 0 && total_bytes / ktom > skipFileDownloadValue) {
                    break;
                }
                outputStream.write(bytes, 0, read);
            }
            if (contentAsStream != null) contentAsStream.close();
            if (outputStream != null) {
                outputStream.flush();
                outputStream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (skipFileDownloadValue > 0 && total_bytes / ktom > skipFileDownloadValue) {
                if (file != null) file.delete();
                return false;
            }
        }
        return true;
    }

    public static boolean writeFile(String href, String file_path) {
        try {
            ReadableByteChannel in = Channels.newChannel(new URL(href).openStream());
            FileChannel out = new FileOutputStream(file_path).getChannel();
            out.transferFrom(in, 0, Long.MAX_VALUE);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }
}
