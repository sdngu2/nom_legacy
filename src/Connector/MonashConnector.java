package Connector;

import Model.Link;
import Model.Unit;
import Utility.ByteCounter;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.ScriptResult;
import com.gargoylesoftware.htmlunit.WebWindow;
import com.gargoylesoftware.htmlunit.html.*;
import com.gargoylesoftware.htmlunit.svg.SvgElement;
import org.apache.commons.codec.binary.Hex;

import javax.swing.*;
import java.io.*;
import java.net.URLDecoder;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Steven Nguyen on 7/07/2014.
 */
public class MonashConnector extends Connector {

    public MonashConnector(JTextArea taData, JTextArea taInfo, JLabel lblStatus, JProgressBar pbProgress) {
        super("http://moodle.vle.monash.edu/login/index.php", taData, taInfo, lblStatus, pbProgress);
    }

    @Override
    public boolean sync(String username, String password) {
        try {
            setProgress(0, 1);
            this.username = username;
            //main_path = System.getProperty("user.home") + separator + "Desktop" + separator + "NOM_" + username;
            main_path = root_path + separator + "NOM_" + username;

            // ##### Essential Base Variables #####
            HtmlPage page;
            HtmlForm form;
            List<?> divs;
            HtmlDivision div;
            HtmlElement element;
            // ####################################

            messageStatus(main_path);
            message("Accessing " + url_login, 0, false);
            wc.getOptions().setJavaScriptEnabled(true);
            page = wc.getPage(url_login);
            byteCount.add(page.asXml().length());
            message(byteCount.getLastAddedAsString('k'), 1, false);

            form = page.getFirstByXPath("//form[@id='loginForm']"); //<form name="login"
            form.getInputByName("UserName").setValueAttribute(username); //<input type="text" name="username"
            form.getInputByName("Password").setValueAttribute(password); //<input type="password" name="password"

            message("Logging in as " + username, 0, false);
            //page = ((HtmlSpan) form.getFirstByXPath("//span[@id='submitButton']")).click(); //<input type="submit" value="login"

            ScriptResult result = page.executeJavaScript("Login.submitLoginRequest();");
            page = (HtmlPage) result.getNewPage();

            byteCount.add(page.asXml().length());
            message(byteCount.getLastAddedAsString('k'), 1, false);

            //divs = page.getByXPath("//p[@class='error-text']"); //<p class="error-text">
            //if (divs.size() > 0) throw new IOException(((HtmlElement) divs.get(0)).asText());
            if (page.asText().contains("Incorrect user ID or password.")) throw new IOException("Incorrect user ID or password.");
            message("Login successful", 1, false);

            page = wc.getPage("http://moodle.vle.monash.edu/my/");

            // @ http://moodle.vle.monash.edu/my/
            message("Retrieving Units", 0, false);
            divs = page.getByXPath("//div[@class='course_title']"); //<div class="course_title">

            System.out.println(page.asXml());

            // Get all units
            for (Object i : divs) {
                div = (HtmlDivision) i;
                String title = div.getHtmlElementsByTagName("a").get(0).getAttribute("title").replaceAll(char_invalid, char_replacement);
                String href = div.getHtmlElementsByTagName("a").get(0).getAttribute("href");

                Pattern p = Pattern.compile("^([A-Z]{3}([0-9]{4} | ))(.+)");
                Matcher m = p.matcher(title);
                if (m.find()) {
                    Unit u = new Unit(m.group(1).trim(), m.group(3).trim(), href);
                    units.add(u);
                } else {
                    System.out.println("Failed: " + title);
                }
            }

            wc.getOptions().setJavaScriptEnabled(false);

            // Collection the Sections and Links in each Unit
            message(units.size() + " unit(s) found", 1, false);
            for (Unit unit : units) {
                message("Accessing: " + unit.getCode() + " - " + unit.getTitle(), 0, false);

                page = wc.getPage(unit.getUrl());
                byteCount.add(page.asXml().length());
                message(byteCount.getLastAddedAsString('k'), 1, false);

                // Activities
                try {
                    if (page.asXml().contains("data-block=\"activity_modules\"")) {
                        element = page.getFirstByXPath("//div[@data-block='activity_modules']");
                        element.removeAttribute("class");
                        element.setAttribute("style","padding-left:20px");
                        //divs = element.getByXPath("//div[@class='content']");
                        //div = (HtmlDivision) divs.get(0);
                        unit.setActivityText(element.asXml());
                    }
                } catch (Exception ex) {
                    // No Activities
                    ex.printStackTrace();
                }

                element = page.getHtmlElementById("region-main");
                String xml = element.asXml();

                // Get all image links
                List<HtmlElement> images = element.getHtmlElementsByTagName("img");
                for (HtmlElement image : images) {
                    String src = image.getAttribute("src"), folder;
                    do {
                        image = (HtmlElement) image.getParentNode();
                    } while (!image.getTagName().equalsIgnoreCase("html") && !image.getId().startsWith("section-"));
                    folder = image.getId() + " " + image.getAttribute("aria-label").trim();

                    unit.addLink(new Link(src, folder, true));
                }

                // Get all other hyperlinks
                List<HtmlElement> links = element.getHtmlElementsByTagName("a");
                for (HtmlElement link : links) {
                    String link_xml = link.asXml(),
                            href = link.getAttribute("href"),
                            title = link.asText().trim().replaceAll(char_invalid, char_replacement),
                            folder = "";

                    do { link = (HtmlElement) link.getParentNode();
                    } while (!link.getTagName().equalsIgnoreCase("html") && !link.getId().startsWith("section-"));

                    if (link.getId().startsWith("section-"))
                        folder = (link.getId() + " " + link.getAttribute("aria-label").trim()).replaceAll(char_invalid, char_replacement);

                    if (href.contains("moodle.vle.monash.edu/mod/folder/")) { // Get folder contents
                        message("Accessing Sub-folder - " + href, 1, false);
                        folder += (folder.isEmpty() ? "" : separator + title);

                        page = wc.getPage(href);
                        byteCount.add(page.asXml().length());
                        message(byteCount.getLastAddedAsString('k'), 2, false);

                        divs = page.getByXPath("//div[@class='box generalbox foldertree']"); //<div class="box generalbox foldertree">
                        if (!divs.isEmpty()) { // Append the folder contents to xml
                            div = (HtmlDivision) divs.get(0);

                            int index = xml.indexOf(link_xml.substring(0, link_xml.indexOf('>')));
                            index = xml.indexOf("</a>", index) + 4;

                            xml = xml.substring(0, index) + "<br>" + div.asXml() + xml.substring(index);
                        }

                        List<HtmlElement> images2 = page.getHtmlElementById("region-main").getHtmlElementsByTagName("img");
                        for (HtmlElement image2 : images2) unit.addLink(new Link(image2.getAttribute("src"), folder, true));
                        List<HtmlElement> links2 = page.getHtmlElementById("region-main").getHtmlElementsByTagName("a");
                        for (HtmlElement link2 : links2) unit.addLink(new Link(link2.getAttribute("href"), folder, false));
                    } else {
                        unit.addLink(new Link(href, folder, false));
                    }
                }
                unit.setText(xml);
                message(unit.getLinks().size() + " link(s) found", 1, false);
            }
            wc.closeAllWindows();
            return true;
        } catch (Exception e) {
            message("Error: " + e.getMessage(), 0, false);
            return false;
        }
    }

    @Override
    public void download(String selectedUnits, int terminateSyncTotalDownloadValue, int skipFileDownloadValue) {
        try {
            // ##### Essential Base Variables #####
            Page link_page;

            setUseInsecureSSL(true);

            dc.incrementLink(1); // To increase CSS file
            for (Unit unit : units) dc.incrementLink(unit.getLinks().size());
            // ####################################

            setProgress(0, dc.getTotalLinks());

            String path_file_css = folder_image_cache + separator + "all.css",
                    url_css = "http://moodle.vle.monash.edu/theme/styles.php/clean/1409189795/all";

            File file_css = new File(main_path + separator + path_file_css);
            if (!file_css.exists()) {
                link_page = wc.getPage(url_css);
                message("CSS URL: " + url_css, 0, false);
                boolean result = FileStreamWriter.writeInputStream(
                        link_page.getWebResponse().getContentAsStream(), main_path + separator + path_file_css, skipFileDownloadValue
                );

                byteCount.add(link_page.getWebResponse().getContentAsString().length());
                message(byteCount.getLastAddedAsString('k'), 1, false);
                if (result) {
                    dc.incrementDownload();
                } else {
                    dc.incrementCancelled();
                    message("Download cancelled", 1, false);
                }
            } else {
                message("Already Downloaded: " + url_css, 0, true);
                dc.incrementExists();
            }

            String file_path;
            for (Unit unit : units) for (Link link : unit.getLinks()) {
                if (link.getUrl().equals("#")) {
                    dc.incrementSkipped();
                    setProgress(dc.downloadProgress());
                    continue;
                }

                // ##### Create folder path
                String hash = new String(Hex.encodeHex(md.digest(link.getUrl().getBytes("UTF-8")))),
                        path_file_dir = unit.getCode() + " " + unit.getTitle() +
                                (link.getSectionName().isEmpty() ? "" : separator + link.getSectionName()),
                        path_file_img = folder_image_cache + separator + hash,
                        path_file_map = folder_mapping + separator + path_file_dir + separator + hash;

                if (link.isImage()) {
                    // Check if image file exists
                    File f = new File(main_path + separator + path_file_img);
                    if (f.exists() && !f.isDirectory())
                        link.setDownloadedPath(path_file_img);
                } else {
                    // Check if file map file exists
                    File f = new File(main_path + separator + path_file_map);
                    if (f.exists() && !f.isDirectory()) {
                        // Get contents containing the actual file path, set downloaded path as that

                        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
                        String data = reader.readLine();

                        f = new File(main_path + separator + data);

                        if (f.exists() && !f.isDirectory())
                            link.setDownloadedPath(data);
                        else
                            message(main_path + separator + data, 0, false);
                    }
                }

                String[] sUnits = selectedUnits.split(",");
                boolean sSkip = true;
                for (String su : sUnits) {
                    if (su.trim().equalsIgnoreCase(unit.getCode())) {
                        sSkip = false;
                        break;
                    }
                }
                if (sSkip) {
                    dc.incrementSkipped();
                    setProgress(dc.downloadProgress());
                    continue;
                }

                link_page = null;
                String href = "";

                // ##### Go to link page and find download
                if (link.getDownloadedPath().equals("")) {
                    try {
                        if (link.isImage()) {
                            message("Image URL: " + link.getUrl(), 0, false);
                            link_page = wc.getPage(link.getUrl());
                            if (link_page instanceof SvgElement) {
                                byteCount.add(((SvgElement) link_page).asXml().length());
                                message(byteCount.getLastAddedAsString('k'), 1, false);
                            }
                        } else if (link.getUrl().contains("moodle.vle.monash.edu/mod/resource/")) {
                            message("Resource URL: " + link.getUrl(), 0, false);
                            link_page = wc.getPage(link.getUrl());

                            if (link_page instanceof HtmlPage) {
                                byteCount.add(((HtmlPage) link_page).asXml().length());
                                message(byteCount.getLastAddedAsString('k') + " (html check r)", 1, false);

                                try {
                                    HtmlElement element;

                                    if (((HtmlPage) link_page).asXml().contains("resourceworkaround")) {
                                        element = (HtmlElement) ((HtmlPage) link_page).getByXPath("//div[@class='resourceworkaround']").get(0);
                                        href = element.getHtmlElementsByTagName("a").get(0).getAttribute("href");

                                        link_page = wc.getPage(href);
                                    } else if (((HtmlPage) link_page).asXml().contains("<iframe")) {
                                        //HtmlInlineFrame frame = ((HtmlPage) link_page).getFirstByXPath("//iframe[@id='resourceobject']");
                                        //link_page = wc.getPage(((HtmlPage) link_page).getFullyQualifiedUrl(frame.getSrcAttribute()));
                                        //href = frame.getSrcAttribute();
                                        element = (HtmlElement) ((HtmlPage) link_page).getByXPath("//iframe[@id='resourceobject']").get(0);
                                        href = element.getAttribute("src");

                                        link_page = wc.getPage(href);
                                    } else {
                                        element = (HtmlElement) ((HtmlPage) link_page).getByXPath("//object[@id='resourceobject']").get(0);
                                        href = element.getAttribute("data");

                                        link_page = wc.getPage(href);
                                    }
                                    System.out.println(href);
                                } catch (IOException e) {
                                    message(e.getMessage(), 1, true);
                                }
                            }
                        } else if (link.getUrl().contains("moodle.vle.monash.edu/pluginfile.php/")) {
                            message("Resource Direct URL: " + link.getUrl(), 0, false);
                            link_page = wc.getPage(link.getUrl());

                            if (link_page instanceof HtmlPage) {
                                byteCount.add(((HtmlPage) link_page).asXml().length());
                                message(byteCount.getLastAddedAsString('k') + " (html check p)", 1, false);

                                String message = "";
                                try {
                                    HtmlElement element = (HtmlElement) ((HtmlPage) link_page).getByXPath("//div[@id='notice']").get(0);
                                    message = ": " + element.asText().trim();
                                } catch (Exception e) {
                                    message(e.getMessage(), 1, true);
                                }
                                message("Error Retrieving Download" + message, 1, false);
                                link_page = null;
                                dc.incrementFailed();
                            }
                        } else if (link.getUrl().startsWith("mailto:")) {
                            message("Skipped Mail URL: " + link.getUrl(), 0, true);
                            dc.incrementSkipped();
                        } else {
                            message("Skipped Unregistered URL: " + link.getUrl(), 0, true);
                            dc.incrementSkipped();
                        }
                    } catch (FailingHttpStatusCodeException e) {
                        message("Error: " + e.getMessage(), 0, false);
                        dc.incrementFailed();
                    } catch (Exception e) {
                        message("Error: Failed to access " + link.getUrl(), 0, false);
                        dc.incrementFailed();
                    }
                } else {
                    message("Already Downloaded: " + link.getUrl(), 0, true);
                    dc.incrementExists();
                }
                // ##### Download the link
                if (link_page != null) {
                    if (link.isImage()) {
                        file_path = main_path + separator + path_file_img;

                        link.setDownloadedPath(path_file_img);
                        messageStatus("DL: " + path_file_img);

                        boolean result = FileStreamWriter.writeInputStream(
                                link_page.getWebResponse().getContentAsStream(), file_path, skipFileDownloadValue
                        );

                        byteCount.add(link_page.getWebResponse().getContentAsString().length());
                        message(byteCount.getLastAddedAsString('k'), 1, false);
                        if (result) {
                            dc.incrementDownload();
                        } else {
                            dc.incrementCancelled();
                            link.setDownloadedPath("");
                            message("Download cancelled", 1, false);
                        }
                    } else {
                        if (!href.isEmpty()) {
                            for (WebWindow w : wc.getWebWindows()) {
                                if (href.equals(w.getEnclosedPage().getUrl().toString())) {
                                    wc.setCurrentWindow(w);
                                    link_page = wc.getCurrentWindow().getEnclosedPage();
                                    break;
                                }
                            }
                        }

                        Pattern p = Pattern.compile("filename=\"([^\"]+)\"");
                        Matcher m = p.matcher(link_page.getWebResponse().getResponseHeaders().toString());
                        if (m.find()){
                            String file_name = URLDecoder.decode(m.group(1), "UTF-8");//m.group(1).replace("%20", " ");
                            file_path = main_path + separator + path_file_dir + separator + file_name;
                            link.setDownloadedPath(path_file_dir + separator + file_name);
                            messageStatus("DL: " + path_file_dir + separator + file_name);

                            boolean result = false;
                            System.out.println(">>> " + file_path);

                            try {
                                result = FileStreamWriter.writeInputStream(
                                        link_page.getWebResponse().getContentAsStream(), file_path, skipFileDownloadValue
                                );

                                byteCount.add(link_page.getWebResponse().getContentAsString().length());
                                message(byteCount.getLastAddedAsString('k') + " \u00bb " + file_name, 1, false);
                            } catch (FileNotFoundException e) {
                                message("Error: File not found exception!", 1, false);
                            }
                            if (result) {
                                dc.incrementDownload();
                            } else {
                                dc.incrementCancelled();
                                link.setDownloadedPath("");
                                message("Download cancelled", 1, false);
                            }

                            if (!href.isEmpty()) {
                                wc.closeAllWindows();
                            }

                            InputStream is = new ByteArrayInputStream((path_file_dir + separator + file_name).getBytes());
                            FileStreamWriter.writeInputStream(is, main_path + separator + path_file_map, 0);
                        } else {
                            message("Unable to identify " + link.getUrl(), 0, false);
                            dc.incrementSkipped();
                        }

                    }
                }
                if (terminateSyncTotalDownloadValue > 0 &&
                        Double.parseDouble(byteCount.getTotalAsString('m').split(" ")[0]) > terminateSyncTotalDownloadValue)
                    break;
                setProgress(dc.downloadProgress());
            }
            // Completion

            messageStatus("Completed");
            message("Completed Download Process", 0, false);
            message("Total Download " + byteCount.getTotalAsString('m'), 1, false);
        } catch (Exception e) {
            message("Error: " + e.getMessage(), 0, false);
        }
    }

    @Override
    public void create_pages(String selectedUnits) {
        try {
            String head, tail = "</body></html>", ext = ".html";
            File file;
            FileStringWriter out;

            String bucket = "";
            for (Unit unit : units) {
                // SKIPPER START
                String[] sUnits = selectedUnits.split(",");
                boolean sSkip = true;
                for (String su : sUnits) {
                    if (su.trim().equalsIgnoreCase(unit.getCode())) {
                        sSkip = false;
                        break;
                    }
                }
                if (sSkip) continue;
                // SKIPPER END
                if (bucket.isEmpty()) {
                    bucket = "<div style=\"padding-left:20px\">";
                } else {
                    bucket += " | ";
                }
                bucket += "<a href=\"./"+ unit.getCode() + ext + "\">";
                bucket += unit.getCode();
                bucket += "</a>";

            }
            bucket += "</div>";

            for (Unit unit : units) {
                // SKIPPER START
                String[] sUnits = selectedUnits.split(",");
                boolean sSkip = true;
                for (String su : sUnits) {
                    if (su.trim().equalsIgnoreCase(unit.getCode())) {
                        sSkip = false;
                        break;
                    }
                }
                if (sSkip) continue;
                // SKIPPER END
                String path_html = main_path + separator + unit.getCode() + ext;
                message("Creating " + path_html, 0, false);
                file = new File(path_html);
                out = new FileStringWriter(file);
                head = "<!DOCTYPE html>" +
                        "<html>" +
                        "<head>" +
                        "<title>" + unit.getCode() + " - " + unit.getTitle() + "</title>" +
                        "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=windows-1252\" />" + // utf-8
                        "<link rel=\"stylesheet\" href=\"" + folder_image_cache + separator + "all.css\">" +
                        "<style type=\"text/css\">" +
                        "section#region-main{width:100%;}" +
                        "form.togglecompletion," +
                        "div#completionprogressid," +
                        "li#topcoll-display-instructions{visibility:hidden;float:right;}" +
                        "div#folder_tree0{margin-top:5px;padding:10px;background:#DEF;}" +
                        "a[href^=\"http://\"],a[href^=\"https://\"]{color:#C40;}" +
                        "body{padding:0}" +
                        "</style>" +
                        "</head>" +
                        "<body>";
                out.write(head);
                out.write(bucket);
                out.write(unit.getActivityText());
                String content = unit.getText();

                for (Link l : unit.getLinks()) {
                    if (!l.getDownloadedPath().isEmpty()) {
                        // message("Replace URL " + l.getUrl() + " to " + l.getDownloadedPath(), 1);
                        content = content.replace(l.getUrl(), l.getDownloadedPath());

                        // Checks SVG Images
                        if (l.isImage()) {
                            File image = new File(main_path + separator + l.getDownloadedPath());

                            Scanner scan = new Scanner(image);
                            while (scan.hasNext())
                                if (scan.next().contains("xmlns=\"http://www.w3.org/2000/svg\"")) {
                                    content = content.replace("<img src=\"" + l.getDownloadedPath(),
                                            "<embed type=\"image/svg+xml\" src=\"" + l.getDownloadedPath());
                                    break;
                                }
                        }
                    }
                }
                //FIXME
                out.write(content.replace("onclick=", "ignored="));
                out.write(tail);
                out.close();

                ByteCounter bc = new ByteCounter();
                bc.add(content.length());
                message("Size: " + bc.getLastAddedAsString('k'), 1, false);
            }
            message("### Synchronize Complete ###", 0, false);
        } catch (Exception e) {
            message("Error: " + e.getMessage(), 0, false);
        }
    }

}
