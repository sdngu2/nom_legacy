package Connector;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;

/**
 * Created by Nc5xb3 on 2/09/2014.
 */
public class FileStringWriter {
    PrintWriter out = null;

    public FileStringWriter(File file) throws Exception {
            if (file.exists() && file.canWrite())
                file.delete();
            file.createNewFile();
            out = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));
    }

    public void write(String str) {
        if (out != null)
            out.println(str);
    }

    public void close() {
        out.flush();
        out.close();
        out = null;
    }
}
