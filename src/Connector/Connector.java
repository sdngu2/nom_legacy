package Connector;

import Model.Unit;
import Utility.ByteCounter;
import Utility.DownloadCounter;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Nc5xb3 on 9/07/2014.
 */
public abstract class Connector {

    String url_login;
    String username = "N/A";
    String main_path;

    ByteCounter byteCount = new ByteCounter();
    DownloadCounter dc = new DownloadCounter();
    List<Unit> units = new LinkedList<Unit>();

    WebClient wc;

    final String note = "### Begin Process ###";

    final String root_path = System.getProperty("user.dir");

    final String folder_image_cache = "image_cache";
    final String folder_mapping = "mapping";
    final String separator = File.separator;

    MessageDigest md;

    final String char_invalid = "[/?<>\\:*|\"]";
    final String char_replacement = "~";//"\u25A0"; //\u2022

    private JTextArea taData, taInfo;
    private JLabel lblStatus;
    private JProgressBar pbProgress;

    public Connector(String url_login, JTextArea taData, JTextArea taInfo, JLabel lblStatus, JProgressBar pbProgress) {
        this.url_login = url_login;
        this.taData = taData;
        this.taInfo = taInfo;
        this.lblStatus = lblStatus;
        this.pbProgress = pbProgress;

        this.taData.setText(this.note);

        message("Initiating web client", 0, false);
        wc = new WebClient(BrowserVersion.getDefault());
        wc.getOptions().setCssEnabled(false);
        wc.getOptions().setJavaScriptEnabled(false);

        wc.getCache().clear();

        //wc.getOptions().setThrowExceptionOnFailingStatusCode(false);
        wc.getOptions().setPrintContentOnFailingStatusCode(false);
        wc.getOptions().setThrowExceptionOnScriptError(false);
        //wc.getOptions().setUseInsecureSSL(true);

        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        message("Initialized. Waiting for login", 1, false);
    }

    abstract public boolean sync(String username, String password) throws Exception;

    abstract public void download(String selectedUnits, int terminateSyncTotalDownloadValue, int skipFileDownloadValue) throws Exception;

    abstract public void create_pages(String selectedUnits) throws Exception;

    void setUseInsecureSSL(boolean useInsecureSSL) {
        wc.getOptions().setUseInsecureSSL(useInsecureSSL);
    }

    public List<Unit> getUnits() {
        return units;
    }

    void message(String message, int level, boolean quiet) {
        message = message.trim();
        if (level-- > 0) message = "^ " + message;
        while (level-- > 0) message = "  " + message;

        if (!quiet) taData.append("\n" + message);

        System.out.println(message);

        updateInfo();
    }

    void messageStatus(String message) {
        String status = message.trim();
        if (dc.getTotalLinks() > 0) status = dc.downloadProgress() + "/" + dc.getTotalLinks() + " " + status;

        System.out.println("Status: " + message);

        lblStatus.setText(status);
    }

    void updateInfo() {
        String s = username + "\n";
        if (dc.getTotalLinks() > 0) s += dc.downloadProgress() + "/" + dc.getTotalLinks() + " links processed\n";
        if (dc.getDownloaded() > 0) s += dc.getDownloaded() + " files downloaded\n";
        if (dc.getExists() > 0) s += dc.getExists() + " already downloaded\n";
        if (dc.getSkipped() > 0) s += dc.getSkipped() + " links skipped\n";
        if (dc.getCancelled() > 0) s += dc.getCancelled() + " downloads cancelled\n";
        if (dc.getFailed() > 0) s += dc.getFailed() + " downloads failed\n";
        if (dc.getTotalLinks() > 0)
            try {
                s += byteCount.getTotalAsString('m') + " of download";
            } catch (IOException e) {
                e.printStackTrace();
            }

        taInfo.setText(s);
    }

    void setProgress(int value) {
        pbProgress.setValue(value);
    }

    void setProgress(int value, int max) {
        setProgress(value);
        pbProgress.setMaximum(max);
    }

    public String getMainPath() {
        return main_path;
    }

}
