package Configuration;

import java.io.*;
import java.util.HashMap;
import java.util.Properties;

/**
 * Created by Nc5xb3 on 12/09/2014.
 */
public class Config {
    final private String filename = "config.properties";

    private Properties config;

    public Config() {
        config = new Properties();

        InputStream in = null;
        try {
            in = new FileInputStream(filename);
            config.load(in);
        } catch (IOException e) {
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                }
            }
        }
    }

    public void put(String key, String value) {
        config.setProperty(key, value);
    }

    public Properties data() {
        return config;
    }

    public void save() throws IOException {
        OutputStream out = new FileOutputStream(filename);
        config.store(out, null);
    }
}
